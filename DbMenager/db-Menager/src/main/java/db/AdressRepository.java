
package db;

import java.util.List;
import model.Adress;

public interface AdressRepository extends Repository<Adress> {
        public List<Adress> withStreet(String surname, PagingInfo page);
	public List<Adress> withCity(String name, PagingInfo page);  
}
