
package db;

import java.util.List;
import model.Client;

public interface ClientRepository extends Repository<Client> {
    
	public List<Client> withSurname(String surname, PagingInfo page);
	public List<Client> withName(String name, PagingInfo page);
}

