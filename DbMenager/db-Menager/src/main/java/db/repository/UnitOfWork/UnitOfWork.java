/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.repository.UnitOfWork;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;
import model.Entity;
import model.EntityState;
public class UnitOfWork implements IUnitOfWork {

    
	private Connection connection;
	
	private Map<Entity, IUnitofWorkRepository> entities = 
			new LinkedHashMap<Entity, IUnitofWorkRepository>();
        
        
        public UnitOfWork(Connection connection) {
		super();
		this.connection = connection;
		
		try {
			connection.setAutoCommit(false);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
    public void commit() {
        for(Entity entity: entities.keySet())
		{
			switch(entity.getState())
			{
			case Changed:
				entities.get(entity).persistUpdate(entity);
				break;
			case Deleted:
				entities.get(entity).persistDelete(entity);
				break;
			case New:
				entities.get(entity).persistAdd(entity);
				break;
			case Unchanged:
				break;
			default:
				break;}
		}
		
		try {
			connection.commit();
			entities.clear();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
    }

    public void rollback() {
       entities.clear();
    }

    public void markAsNew(Entity entity, IUnitofWorkRepository repository) {
        entity.setState(EntityState.New);
	entities.put(entity, repository);
    }

    public void markAsDirty(Entity entity, IUnitofWorkRepository repository) {
        entity.setState(EntityState.Changed);
        entities.put(entity, repository);
    }

    public void markAsDeleted(Entity entity, IUnitofWorkRepository repository) {
        entity.setState(EntityState.Deleted);
	entities.put(entity, repository);
    }
    
}
