/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.repository.UnitOfWork;
import model.Entity;
public interface IUnitOfWork {
    public void commit();
	public void rollback();
	public void markAsNew(Entity entity, IUnitofWorkRepository repository);
	public void markAsDirty(Entity entity, IUnitofWorkRepository repository);
	public void markAsDeleted(Entity entity, IUnitofWorkRepository repository);
}
