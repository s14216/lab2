
package db.catalog;

import db.AdressRepository;
import db.ClientRepository;
import db.OrderItemRepository;
import db.OrderRepository;


public interface RepositoryCatalog {
public OrderRepository order();  
public AdressRepository adress();
public ClientRepository client();
public OrderItemRepository orderItem (); 
}
